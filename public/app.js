const clientId = 'a7cdb551aff74e618458c6ef473f7ff0'; // unsere clientId
const redirectUrl = 'https://socialweb.master.kwmhgb.at/gruppe2/Kotzian/2%20Semester%20Social%20Web/Spotify_Example/public/'; // unsere Redirect-URL - muss localhost URL und/oder HTTPS sein

const authorizationEndpoint = "https://accounts.spotify.com/authorize";
const tokenEndpoint = "https://accounts.spotify.com/api/token";
const scope = 'user-read-private user-read-email';

// Datenstruktur, die das aktuell aktive Token verwaltet und in localStorage speichert
const currentToken = {
    get access_token() { return localStorage.getItem('access_token') || null; },
    get refresh_token() { return localStorage.getItem('refresh_token') || null; },
    get expires_in() { return localStorage.getItem('expires_in') || null },
    get expires() { return localStorage.getItem('expires') || null },

    save: function (response) {
        const { access_token, refresh_token, expires_in } = response;
        localStorage.setItem('access_token', access_token);
        localStorage.setItem('refresh_token', refresh_token);
        localStorage.setItem('expires_in', expires_in);

        const now = new Date();
        const expiry = new Date(now.getTime() + (expires_in * 1000));
        localStorage.setItem('expires', expiry);
    }
};

// Beim Laden der Seite wird versucht, den Authentifizierungscode aus der aktuellen Browser-Such-URL zu holen
//der Code wird nach erfolgreichem Login und Autorisierung and die Redirect-URL angehängt
window.onload = async function() {
    const args = new URLSearchParams(window.location.search);
    const code = args.get('code');

    // Wenn wir einen Authentifizierungscode finden, befinden wir uns in einem Callback und führen einen Token-Austausch durch
    if (code) {
        //Token mit Authentifizierungscode wird geholt
        const token = await getToken(code);
        //Wenn Token erfolgreich abgerufen ist, dann
        if (token.access_token) {
            //Token wird im localStorage gespeichert
            currentToken.save(token);

            // URL Objekt wird erstellt, wo die aktuelle URL gespeichert ist
            const url = new URL(window.location.href);
            url.searchParams.delete("code"); //Authentifizierungscode wird aus der URL gelöscht

            //URL wird aktualisiert, ohne dass Authentofizierungscode in der URL sichtbar ist
            const updatedUrl = url.search ? url.href : url.href.replace('?', '');
            //URL wird im Browser aktualisiert, ohne dass jetzt der Authentifizierungscode sichtbar ist
            window.history.replaceState({}, document.title, updatedUrl);
        } else {
            console.error("Failed to exchange code for token:", token);
        }
    }

    // Wenn wir ein Token haben, sind wir eingeloggt, also holen wir die Benutzerdaten und rendern die eingeloggte Vorlage
    if (currentToken.access_token) {
        const userData = await getUserData();
        if (userData) {
            renderTemplate("main", "logged-in-template", userData);
            renderTemplate("oauth", "oauth-template", currentToken);
        } else {
            console.error("Failed to fetch user data");
        }
    } else {
        // Andernfalls sind wir nicht eingeloggt, also rendern wir die Login-Vorlage
        renderTemplate("main", "login");
    }
};

async function redirectToSpotifyAuthorize() {
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const randomValues = crypto.getRandomValues(new Uint8Array(64));
    const randomString = randomValues.reduce((acc, x) => acc + possible[x % possible.length], "");

    const code_verifier = randomString;
    const data = new TextEncoder().encode(code_verifier);
    const hashed = await crypto.subtle.digest('SHA-256', data);

    const code_challenge_base64 = btoa(String.fromCharCode(...new Uint8Array(hashed)))
        .replace(/=/g, '')
        .replace(/\+/g, '-')
        .replace(/\//g, '_');

    window.localStorage.setItem('code_verifier', code_verifier);

    const authUrl = new URL(authorizationEndpoint)
    const params = {
        response_type: 'code',
        client_id: clientId,
        scope: scope,
        code_challenge_method: 'S256',
        code_challenge: code_challenge_base64,
        redirect_uri: redirectUrl,
    };

    authUrl.search = new URLSearchParams(params).toString();
    window.location.href = authUrl.toString(); // User zum authorization server für Login umleiten
}

// Spotify API Aufrufe
async function getToken(code) {
    const code_verifier = localStorage.getItem('code_verifier');

    const response = await fetch(tokenEndpoint, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: new URLSearchParams({
            client_id: clientId,
            grant_type: 'authorization_code',
            code: code,
            redirect_uri: redirectUrl,
            code_verifier: code_verifier,
        }),
    });

    const data = await response.json();
    if (!response.ok) {
        console.error("Error fetching token:", data);
    }
    return data;
}

async function refreshToken() {
    const response = await fetch(tokenEndpoint, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: new URLSearchParams({
            client_id: clientId,
            grant_type: 'refresh_token',
            refresh_token: currentToken.refresh_token
        }),
    });

    const data = await response.json();
    if (!response.ok) {
        console.error("Error refreshing token:", data);
    }
    return data;
}

async function getUserData() {
    const response = await fetch("https://api.spotify.com/v1/me", {
        method: 'GET',
        headers: { 'Authorization': 'Bearer ' + currentToken.access_token },
    });

    if (response.ok) {
        return await response.json();
    } else {
        const data = await response.json();
        console.error("Error fetching user data:", data);
        return null;
    }
}

// Klick-Handler
async function loginWithSpotifyClick() {
    await redirectToSpotifyAuthorize();

}

async function logoutClick() {
    localStorage.clear();
    window.location.href = redirectUrl;
}

async function refreshTokenClick() {
    const token = await refreshToken();
    if (token.access_token) {
        currentToken.save(token);
        renderTemplate("oauth", "oauth-template", currentToken);
    } else {
        console.error("Failed to refresh token:", token);
    }
}

// 1800000 Millisekunden entsprechen 30 Minuten
setInterval(refreshTokenClick, 1800000);

// HTML-Vorlage Rendering mit grundlegender Datenbindung
function renderTemplate(targetId, templateId, data = null) {
    const template = document.getElementById(templateId);
    const clone = template.content.cloneNode(true);

    const elements = clone.querySelectorAll("*");
    elements.forEach(ele => {
        const bindingAttrs = [...ele.attributes].filter(a => a.name.startsWith("data-bind"));

        bindingAttrs.forEach(attr => {
            const target = attr.name.replace(/data-bind-/, "").replace(/data-bind/, "");
            const targetType = target.startsWith("onclick") ? "HANDLER" : "PROPERTY";
            const targetProp = target === "" ? "innerHTML" : target;

            const prefix = targetType === "PROPERTY" ? "data." : "";
            const expression = prefix + attr.value.replace(/;\n\r\n/g, "");

            try {
                ele[targetProp] = targetType === "PROPERTY" ? eval(expression) : () => { eval(expression) };
                ele.removeAttribute(attr.name);
            } catch (ex) {
                console.error(`Error binding ${expression} to ${targetProp}`, ex);
            }
        });
    });

    const target = document.getElementById(targetId);
    target.innerHTML = "";
    target.appendChild(clone);

    // Klick-Handler binden
    const loginButton = document.getElementById('login-button');
    if (loginButton) loginButton.addEventListener('click', loginWithSpotifyClick);

    const refreshTokenButton = document.getElementById('refresh-token-button');
    if (refreshTokenButton) refreshTokenButton.addEventListener('click', refreshTokenClick);

    const logoutButton = document.getElementById('logout-button');
    console.log("Button geändert")
    //logoutButton.classList.add('logout-button');
    if (logoutButton) logoutButton.addEventListener('click', logoutClick);
}

// Spotify-API-Aufruf zur Suche nach Wiedergabelisten anhand des Buchtitels
async function searchSpotifyPlaylists(query) {
    const response = await fetch(`https://api.spotify.com/v1/search?q=${encodeURIComponent(query)}&type=playlist`, {
        method: 'GET',
        headers: { 'Authorization': 'Bearer ' + currentToken.access_token },
    });

    if (response.ok) {
        const data = await response.json();
        return data.playlists.items; // Liste an Playlists zurückgeben
    } else {
        const data = await response.json();
        console.error("Error fetching Spotify playlists:", data);
        return [];
    }
}

// Spotify-API-Aufruf zum Abrufen von Titeln einer Wiedergabeliste
async function getPlaylistTracks(playlistId) {
    const response = await fetch(`https://api.spotify.com/v1/playlists/${playlistId}/tracks`, {
        method: 'GET',
        headers: { 'Authorization': 'Bearer ' + currentToken.access_token },
    });

    if (response.ok) {
        const data = await response.json();
        return data.items; // Returns the list of tracks
    } else {
        const data = await response.json();
        console.error("Error fetching playlist tracks:", data);
        return [];
    }
}

// Funktion zum Rendern von Spotify-Wiedergabelisten in das DOM
function renderSpotifyPlaylists(playlists) {
    let DIVSpotifyHeading = document.createElement('div');
    DIVSpotifyHeading.classList.add("Spotify-Heading");
    DIVSpotifyHeading.innerHTML = 'Deine empfohlenen Spotify-Playlists';

    let DIVLoginInfo = document.createElement('div');
    DIVLoginInfo.id = "Login-Spotify-Info";
    DIVLoginInfo.classList.add("Login-Info");
    DIVLoginInfo.innerHTML = `
        <p>Bitte logge dich bei Spotify ein, um Playlists empfohlen zu bekommen.</p>`;

    let spotifyResults = document.querySelector("#Spotify-Ergebnisse");
    spotifyResults.classList.remove("hide");
    spotifyResults.classList.add("spotify-detail");
    spotifyResults.innerHTML = ''; // Vorherige Ergebnisse löschen

    spotifyResults.appendChild(DIVSpotifyHeading);
    spotifyResults.appendChild(DIVLoginInfo);

    // Überprüfen, ob der User eingeloggt ist
    if (currentToken.access_token) {
        DIVLoginInfo.classList.add("hide"); // Login-Info verstecken, falls man bereits eingeloggt ist
    } else {
        DIVLoginInfo.classList.remove("hide"); // Login-Info anzeigen, falls man nicht eingeloggt ist.
    }

    // Maximale Anzahl der anzuzeigenden Wiedergabelisten festlegen (10)
    const maxPlaylists = 10;
    let count = 0;

    playlists.forEach(playlist => {
        if (count >= maxPlaylists) return; // Schleife beenden, wenn die maximale Anzahl an Wiedergabelisten erreicht wurde

        let playlistElement = document.createElement('div');
        playlistElement.classList.add('playlistElement');
        playlistElement.innerHTML = `
            <div class="coverANDdetails">
                <div class="playlist-cover">
                    <img src="${playlist.images[0].url}" alt="${playlist.name}">
                </div>
                <div class="playlist-details">
                    <h3>${playlist.name}</h3>
                    <p>By: ${playlist.owner.display_name}</p>
                    <a href="${playlist.external_urls.spotify}" target="_blank">Open in Spotify</a>
                    <p class="dropdown toggle-tracks">▼ Songs anzeigen</p>
                 </div>
             </div>
            <div class="tracks hide"></div>`;
        spotifyResults.appendChild(playlistElement);

        playlistElement.querySelector('.toggle-tracks').addEventListener('click', async (event) => {
            event.stopPropagation(); // Prevent default action
            await togglePlaylistTracks(playlist.id, playlistElement);
        });
        count++;
    });
    spotifyResults.classList.remove("hide");
}

// Funktion zum Umschalten der Sichtbarkeit von Titeln der Playlists
async function togglePlaylistTracks(playlistId, playlistElement) {
    let tracksContainer = playlistElement.querySelector('.tracks');
    let toggleElement = playlistElement.querySelector('.toggle-tracks');

    if (tracksContainer.classList.contains('hide')) {
        //Wenn Tracks unsichtbar sind, diese einblenden
        let tracks = await getPlaylistTracks(playlistId);
        tracksContainer.innerHTML = ''; // Vorherige Tracks clearen
        tracks.forEach(item => {
            let track = item.track;
            let trackElement = document.createElement('div');
            trackElement.classList.add('trackElement');
            trackElement.innerHTML = `
                <div class="track-name">${track.name}</div>
                <div class="track-artist">${track.artists.map(artist => artist.name).join(', ')}</div>
                <div class="track-album">${track.album.name}</div>
                <a href="${track.external_urls.spotify}" target="_blank">Play on Spotify</a>`;
            tracksContainer.appendChild(trackElement);
        });
        tracksContainer.classList.remove('hide');
        toggleElement.textContent = '▲ Songs ausblenden'; // Ändere Text zu 'Songs ausblenden'
    } else {
        //Wenn Tracks sichtbar sind, diese ausblenden
        tracksContainer.classList.add('hide');
        toggleElement.textContent = '▼ Songs anzeigen'; // Ändere Text zurück zu 'Songs anzeigen'
    }
}

// Ereignis-Listener für Buchklick zum Abrufen von Spotify-Wiedergabelisten
document.querySelector("#Suchergebnisse").addEventListener("click", async function(event) {
    if (event.target.closest(".bookElement")) {
        const bookTitle = event.target.closest(".bookElement").querySelector(".book-details h2").innerText;
        const playlists = await searchSpotifyPlaylists(bookTitle);
        renderSpotifyPlaylists(playlists);
    }
});
