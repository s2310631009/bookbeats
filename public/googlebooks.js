document.addEventListener("DOMContentLoaded", function() {
    let searchButton = document.querySelector("#search");
    let searchInput = document.querySelector("#such-eingabefeld");

    searchButton.addEventListener("click", function() {
        executeSearch();
    });

    // Event-Listener für Enter-Taste in der Suchleiste
    searchInput.addEventListener("keyup", function(event) {
        if (event.key === "Enter") {
            executeSearch();
        }
    });

    // Funktion zur Ausführung der Suche
    function executeSearch() {
        clearSearchResults();
        clearSpotifyResults();
        let searchQuery = searchbar(); // Ruft die searchbar() Funktion auf und speichert das Ergebnis
        searchBooks(searchQuery); // Übergibt das Ergebnis der searchbar() Funktion an searchBooks()
    }

    // Event-Listener für Klick auf ein Buchelement hinzufügen
    document.querySelector("#Suchergebnisse").addEventListener("click", async function(event) {
        if (event.target.closest(".bookElement")) {
            // Alle Buchelemente ausblenden
            let allBooks = document.querySelectorAll(".bookElement");
            allBooks.forEach(function(book) {
                book.classList.add("hide");
            });

            // Das angeklickte Buch anzeigen
            let clickedBook = event.target.closest(".bookElement");
            clickedBook.classList.remove("hide");
            clickedBook.classList.remove("bookElement");

            clickedBook.querySelector("#bookDescription").classList.remove("hide");
            clickedBook.querySelector("#h2ChosenBook").classList.remove("hide");

            document.querySelector(".suchergebnisse").classList.remove("suchergebnisse");
            document.querySelector("#Suchergebnisse").classList.add("chosen-book-overview");
            document.querySelector(".bookElement").classList.remove("bookElement");
            document.querySelector(".book-details").classList.remove("book-details");

            // Spotify Playlists anzeigen
            const bookTitle = clickedBook.querySelector(".book-details h2").innerText;
            const playlists = await searchSpotifyPlaylists(bookTitle);
            renderSpotifyPlaylists(playlists);
        }
    });
});

//================== GOOGLE BOOKS ==================

function searchBooks(query) {
    // API-Endpunkt für die Buchsuche
    let apiKey = 'AIzaSyBX4AaEvmf5lGErDFCFCoipBnnZS7TyXgw';
    let apiUrl = 'https://www.googleapis.com/books/v1/volumes?q=' + encodeURIComponent(query) + '&key=' + apiKey;

    // API-Anfrage durchführen
    $.get(apiUrl, function(data) {
        // Ergebnisse verarbeiten
        if (data.items && data.items.length > 0) {
            for (let i = 0; i < Math.min(data.items.length, 10); i++) {
                let book = data.items[i];

                // HTML-Elemente für jedes Buch erstellen und dem Container hinzufügen
                let suchergebnisse = document.querySelector("#Suchergebnisse");
                let bookElement = document.createElement('div');
                bookElement.classList.add('bookElement');
                bookElement.innerHTML = `
            <div id="h2ChosenBook" class="hide">Dein ausgewähltes Buch</div>
            <div class="book-cover">
                <img src="${book.volumeInfo.imageLinks.thumbnail}">
            </div>
            <div class="book-details">
                <h2>${book.volumeInfo.title}</h2>
                <p>Autor(en): ${book.volumeInfo.authors ? book.volumeInfo.authors.join(", ") : "Unbekannt"}</p>
                <p id="bookDescription" class="hide">Beschreibung: ${book.volumeInfo.description ? book.volumeInfo.description : "Keine Beschreibung verfügbar"}</p>
            </div>`;
                suchergebnisse.appendChild(bookElement);
            }
        } else {
            resultsContainer.innerHTML = 'Keine Ergebnisse gefunden.';
        }
        document.querySelector("#Suchergebnisse").classList.remove("hide");
        document.querySelector("#Suchergebnisse").classList.add("suchergebnisse");
    });
}

// Zugriff auf die Suchleiste
function searchbar() {
    let searchstring = document.querySelector("#such-eingabefeld").value;
    console.log(searchstring);
    return searchstring;
}

// Vor dem Hinzufügen neuer Ergebnisse werden alle vorhandenen Ergebnisse gelöscht
function clearSearchResults() {
    console.log("clearSearchResults wird aufgerufen");
    let searchResultsContainer = document.querySelector("#Suchergebnisse");
    searchResultsContainer.innerHTML = ''; // Alle vorhandenen Suchergebnisse entfernen
}

function clearSpotifyResults() {
    let searchSpotifyResultsContainer = document.querySelector("#Spotify-Ergebnisse");
    searchSpotifyResultsContainer.innerHTML = ''; // Alle vorhandenen Spotifysuchergebnisse entfernen
    document.querySelector("#Spotify-Ergebnisse").classList.add("hide");
}